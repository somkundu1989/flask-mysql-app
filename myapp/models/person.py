from datetime import datetime
from myapp.config import db, ma
from marshmallow_sqlalchemy import ModelSchema

class Person(db.Model):
    __tablename__ = "persons"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    email = db.Column(db.String(255))
    dob = db.Column(db.String(32))
    place = db.Column(db.String(32))
    timestamp = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow
    )

class PersonSchema(ModelSchema):
    class Meta:
        model = Person
        sql_session = db.session