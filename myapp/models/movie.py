from datetime import datetime
from myapp.config import db, ma
from marshmallow_sqlalchemy import ModelSchema

class Movie(db.Model):
    __tablename__ = "movies"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    release_date = db.Column(db.String(32))
    sensored = db.Column(db.String(255))
    casts = db.Column(db.String(500))
    genres = db.Column(db.String(500))
    timestamp = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow
    )

class MovieSchema(ModelSchema):
    class Meta:
        model = Movie
        sql_session = db.session