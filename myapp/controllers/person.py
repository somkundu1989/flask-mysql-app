from flask import Blueprint, jsonify, request
from myapp.config import db
from myapp.models.person import Person, PersonSchema

api02 = Blueprint('_api02', __name__)

@api02.route("/")
def all():

    people = Person.query.order_by(Person.id).all()
    person_schema = PersonSchema(many=True)
    list = person_schema.dump(people)

    return jsonify({'data': list, 'text': 'list of persons'}), 200

@api02.route("/", methods=['POST'])
def create():
    person = request.get_json()

    existing_person = Person.query.filter(Person.email == person.get("email")).one_or_none()

    # Can we insert this person?
    if existing_person is None:

        # Create a person instance using the schema and the passed in person
        schema = PersonSchema()
        new_person = schema.load(person, session=db.session)

        # Add the person to the database
        db.session.add(new_person)
        db.session.commit()

        # Serialize and return the newly created person in the response
        data = schema.dump(new_person)

        # return data, 201, "created"
        return jsonify({'data': data, 'text': 'person created'}), 200

    # Otherwise, nope, person exists already
    else:
        data = "Person {name} {email} exists already".format(name=person.get("name"), email=person.get("email") )
        # return data, 409, "created"
        return jsonify({'data': data, 'text': 'person can\'t be created'}), 200


@api02.route("/<id>", methods=['PUT'])
def update(id):
    person = request.get_json()

    # turn the passed in person into a db object
    schema = PersonSchema()
    update_person = schema.load(person, session=db.session)

    # Set the id to the person we want to update
    update_person.id = id

    # merge the new object into the old and commit it to the db
    db.session.merge(update_person)
    db.session.commit()

    # return updated person in the response
    data = schema.dump(update_person)

    # return data, 200, "updated"
    return jsonify({'data': data, 'text': 'person updated'})



@api02.route("/<id>", methods=['DELETE'])
def delete(id):
    # print(id)
    # Get the person requested
    person = Person.query.filter(Person.id == id).one_or_none()

    db.session.delete(person)
    db.session.commit()

    # return "deleted"
    return jsonify({'text': 'person deleted'})


@api02.route("/<id>")
def fetch(id):
    # print(id)
    # Get the person requested
    person = Person.query.filter(Person.id == id).one_or_none()


    person_schema = PersonSchema()
    data = person_schema.dump(person)
    # return "fetched"
    return jsonify({'data': data, 'text': 'person fetched'})