from flask import Blueprint, jsonify, request
from myapp.config import db
from myapp.models.movie import Movie, MovieSchema

api01 = Blueprint('_api01', __name__)

@api01.route("/")
def all():

    movie = Movie.query.order_by(Movie.id).all()
    movie_schema = MovieSchema(many=True)
    list = movie_schema.dump(movie)

    return jsonify({'data': list, 'text': 'list of movies'}), 200

@api01.route("/", methods=['POST'])
def create():
    movie = request.get_json()

    existing_movie = Movie.query.filter(Movie.name == movie.get("name")).filter(Movie.release_date == movie.get("release_date")).one_or_none()

    # Can we insert this movie?
    if existing_movie is None:

        # Create a movie instance using the schema and the passed in movie
        schema = MovieSchema()
        new_movie = schema.load(movie, session=db.session)

        # Add the movie to the database
        db.session.add(new_movie)
        db.session.commit()

        # Serialize and return the newly created movie in the response
        data = schema.dump(new_movie)

        # return data, 201, "created"
        return jsonify({'data': data, 'text': 'movie created'}), 200

    # Otherwise, nope, movie exists already
    else:
        data = "Movie {name} {release_date} exists already".format(name=movie.get("name"), release_date=movie.get("release_date") )
        # return data, 409, "created"
        return jsonify({'data': data, 'text': 'movie can\'t be created'}), 200


@api01.route("/<id>", methods=['PUT'])
def update(id):
    movie = request.get_json()

    # turn the passed in movie into a db object
    schema = MovieSchema()
    update_movie = schema.load(movie, session=db.session)

    # Set the id to the movie we want to update
    update_movie.id = id

    # merge the new object into the old and commit it to the db
    db.session.merge(update_movie)
    db.session.commit()

    # return updated movie in the response
    data = schema.dump(update_movie)

    # return data, 200, "updated"
    return jsonify({'data': data, 'text': 'movie updated'})



@api01.route("/<id>", methods=['DELETE'])
def delete(id):
    # print(id)
    # Get the movie requested
    movie = Movie.query.filter(Movie.id == id).one_or_none()

    db.session.delete(movie)
    db.session.commit()

    # return "deleted"
    return jsonify({'text': 'movie deleted'})


@api01.route("/<id>")
def fetch(id):
    # print(id)
    # Get the movie requested
    movie = Movie.query.filter(Movie.id == id).one_or_none()


    movie_schema = MovieSchema()
    data = movie_schema.dump(movie)
    # return "fetched"
    return jsonify({'data': data, 'text': 'movie fetched'})