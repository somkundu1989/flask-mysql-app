from functools import wraps
from flask import request, Response, g

def auth_decorator():
    def _auth_decorator(f):
        @wraps(f)
        def __auth_decorator(*args, **kwargs):
            # just do here everything what you need
            # print('before home')
            token = request.args.get('token')
            # print( body,'')


            if token == 'TestUser':
                token = 'test token here!'

                g.token = token

                return f(*args, **kwargs)

            return Response('Authorization failed',mimetype="application/json", status=200)

        return __auth_decorator
    return _auth_decorator
